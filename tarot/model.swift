//
//  model.swift
//  tarot
//
//  Created by Steph Ridnell on 27/12/17.
//  Copyright © 2017 StephRidnell. All rights reserved.
//

import Foundation

class Model {
  
  var currentCard = MajorArcanaCard.fool
  
  var deckOfCards:DeckOfCards
  
  /* Here we use a Struct to hold the instance of the model i.e itself*/
  private struct Static {
    static var instance: Model?
  }
  
  /* This is a class variable allowing me to access it without first 
   instantiating the model Now we can retrieve the model without instantiating 
   it directly var model = Model.sharedInstance */
  
  class var sharedInstance: Model {
    if !(Static.instance != nil) {
      Static.instance = Model()
    }
    return Static.instance!
  }
  
  // Populate the model with a set of Major Arcana Cards
  private init() {
    deckOfCards = DeckOfCards()
  }
  
  // Return the oracles response to the question posed by the user
  func respond()->String {
    
    // Returns a random integer within the range of indexes for the answers array
    let response: Int = Int(arc4random_uniform(UInt32(deckOfCards.deckOfCards.count)))
    
    currentCard = Array(deckOfCards.deckOfCards)[response]
    
    return currentCard.interpretation
  }
}
